﻿using System;
using Services;

namespace Class1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var userName = Name.GetUserName();
            var userDOB = int.Parse(Age.GetUserDOB());
            var currentYear = DateTime.UtcNow.Year;
            Console.WriteLine($"Your name is : { userName }.");
            Console.WriteLine($"You are { currentYear - userDOB } year(s) old.");
            Console.ReadLine();
        }
    }
}
